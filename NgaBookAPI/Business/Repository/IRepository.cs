﻿using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Business.Repository
{
    public interface IRepository<T> where T : BaseModel
    {
        IEnumerable<T> GetAll();

        T GetById(int id);

        void Add(T entity);

        void Delete(T entity);

        bool Exists(int id);

        void SaveAll();
    }
}