﻿using AutoMapper;
using Business.Dto;
using Business.Exceptions;
using Business.Repository;
using Models;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Business.Service
{
    public class UserService : Repository<User>, IUserService
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public UserService(ApplicationDbContext context, IMapper mapper) : base(context)
        {
            _context = context;
            _mapper = mapper;
        }

        public User AddUser(UserForInsert user)
        {
            var userToAdd = _mapper.Map<User>(user);

            userToAdd.Password = EncryptPassword(user.Password);
            userToAdd.Role = (Role)Enum.Parse(typeof(Role), user.Role, true);

            Add(userToAdd);

            return userToAdd;
        }

        public User Update(int id, UserForInsert user)
        {
            var userToUpdate = GetById(id);
            _mapper.Map(user, userToUpdate);

            userToUpdate.Password = EncryptPassword(user.Password);
            userToUpdate.Role = (Role)Enum.Parse(typeof(Role), user.Role, true);

            _context.SaveChanges();

            return GetById(id);
        }

        public bool UsernameExists(string username)
        {
            return _context.Users.Any(u => string.Equals(u.Username, username));
        }

        private string EncryptPassword(string password)
        {
            var md5 = new MD5CryptoServiceProvider();

            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(password));

            return Convert.ToBase64String(md5.Hash);
        }
    }
}