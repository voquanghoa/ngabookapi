﻿using Business.Dto;
using Business.Repository;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Business.Service
{
    public interface IUserService : IRepository<User>
    {
        User AddUser(UserForInsert user);

        User Update(int id, UserForInsert user);

        bool UsernameExists(string username);
    }
}