﻿using AutoMapper;
using Business.Dto;
using Business.Exceptions;
using Business.Utils;
using Models;
using System;
using System.Linq;

namespace Business.Service
{
    public class AuthService : IAuthService
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public AuthService(ApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public UserForDetailed Login(UserForLogin userForLogin)
        {
            var user = _context.Users.FirstOrDefault(u => string.Equals(u.Username, userForLogin.Username))
                ?? throw new BadRequestException("User Doesn't Exist");

            if (!AuthUtils.VerifyPassword(userForLogin.Password, user.Password))
            {
                throw new BadRequestException("Wrong password");
            }

            return _mapper.Map<UserForDetailed>(user);
        }

        public UserForDetailed GetUser(string username)
        {
            var user = _context.Users.FirstOrDefault(u => string.Equals(u.Username, username))
                ?? throw new BadRequestException("User Doesn't Exist");

            return _mapper.Map<UserForDetailed>(user);
        }
    }
}