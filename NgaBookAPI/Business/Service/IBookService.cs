﻿using Business.Dto;
using Business.Repository;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Business.Service
{
    public interface IBookService : IRepository<Book>
    {
        void MapAuthorAndCategory(BookForInsert src, Book dest);

        Book UpdateBook(int id, BookForInsert book);

        User FindOwner(int userId);

        UserForDetailed GetUser(string username);
    }
}