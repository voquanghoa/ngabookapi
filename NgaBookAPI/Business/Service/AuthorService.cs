﻿using AutoMapper;
using Business.Dto;
using Business.Repository;
using Models;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Business.Service
{
    public class AuthorService : Repository<Author>, IAuthorService
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public AuthorService(ApplicationDbContext context, IMapper mapper) : base(context)
        {
            _context = context;
            _mapper = mapper;
        }

        public Author Update(int id, AuthorForInsert author)
        {
            var authorToUpdate = GetById(id);
            _mapper.Map(author, authorToUpdate);

            _context.SaveChanges();

            return GetById(id);
        }
    }
}