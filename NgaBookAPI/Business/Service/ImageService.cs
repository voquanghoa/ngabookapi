﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace Business.Service
{
    public class ImageService
    {
        private static readonly int MaxImageWidth = 800;
        private static readonly int MaxImageHeight = 800;
        private static readonly string ImagePath = ConfigurationManager.AppSettings["CoverImagesUrl"];

        public static string SaveImage(byte[] content)
        {
            var fileName = $"{Guid.NewGuid()}.jpg";
            var filePath = Path.Combine(GetImageFolder(), fileName);

            using (var memoryStream = new MemoryStream(content))
            {
                using (var img = new Bitmap(memoryStream))
                {
                    if (img.Width > MaxImageWidth || img.Height > MaxImageHeight)
                    {
                        var ratio = Math.Min((double)MaxImageHeight / img.Height, (double)MaxImageWidth / img.Width);
                        using (var croppedImg = new Bitmap(img, (int)(img.Width * ratio), (int)(img.Height * ratio)))
                        { 
                            croppedImg.Save(filePath, ImageFormat.Jpeg);
                        }
                    }

                    else
                    {
                        img.Save(filePath, ImageFormat.Jpeg);
                    }
                }
            }

            return fileName;
        }

        private static string GetImageFolder()
        {
            var physicalPath = HttpContext.Current.Server.MapPath(ImagePath);
            if (!Directory.Exists(ImagePath))
            {
                Directory.CreateDirectory(physicalPath);
            }

            return physicalPath;
        }
    }
}
