﻿using Business.Dto;
using Business.Repository;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Business.Service
{
    public interface ICategoryService : IRepository<Category>
    {
        Category Update(int id, CategoryForInsert category);
    }
}