﻿using AutoMapper;
using Business.Dto;
using Business.Repository;
using Models;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Business.Service
{
    public class BookService : Repository<Book>, IBookService
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public BookService(ApplicationDbContext context, IMapper mapper) : base(context)
        {
            _context = context;
            _mapper = mapper;
        }

        private Author FindAuthor(int authorId)
        {
            return _context.Authors.Find(authorId);
        }

        private Category FindCategory(int categoryId)
        {
            return _context.Categories.Find(categoryId);
        }

        public void MapAuthorAndCategory(BookForInsert src, Book dest)
        {
            var id = src.Author.Id;
            dest.AuthorId = id;
            dest.Author = FindAuthor(id);

            foreach (var category in src.Categories)
            {
                dest.Categories.Add(FindCategory(category.Id));
            }
        }

        public Book UpdateBook(int id, BookForInsert book)
        {
            var bookToUpdate = GetById(id);

            _mapper.Map(book, bookToUpdate);
            MapAuthorAndCategory(book, bookToUpdate);

            _context.SaveChanges();

            return GetById(id);
        }

        public User FindOwner(int userId)
        {
            return _context.Users.Find(userId);
        }

        public UserForDetailed GetUser(string username)
        {
            var user = _context.Users.FirstOrDefault(u => u.Username == username);
            return _mapper.Map<UserForDetailed>(user);
        }
    }
}