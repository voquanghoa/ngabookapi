﻿using Business.Dto;
using Business.Repository;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Service
{
    public interface IAuthorService : IRepository<Author>
    {
        Author Update(int id, AuthorForInsert author);
    }
}
