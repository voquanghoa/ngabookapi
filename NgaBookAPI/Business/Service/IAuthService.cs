﻿using Business.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Business.Service
{
    public interface IAuthService
    {
        UserForDetailed Login(UserForLogin userForLogin);

        UserForDetailed GetUser(string username);
    }
}