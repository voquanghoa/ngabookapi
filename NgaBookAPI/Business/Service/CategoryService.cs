﻿using AutoMapper;
using Business.Dto;
using Business.Repository;
using Models;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Business.Service
{
    public class CategoryService : Repository<Category>, ICategoryService
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public CategoryService(ApplicationDbContext context, IMapper mapper) : base(context)
        {
            _context = context;
            _mapper = mapper;
        }

        public Category Update(int id, CategoryForInsert category)
        {
            var categoryToUpdate = GetById(id);

            _mapper.Map(category, categoryToUpdate);
            _context.SaveChanges();

            return GetById(id);
        }
    }
}