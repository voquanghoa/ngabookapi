﻿using Business.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace Business.SecurityModels
{
    public class UserPrincipal : IPrincipal
    {
        public IIdentity Identity { get; }

        public UserForDetailed User { get; }

        public bool IsInRole(string role)
        {
            return string.Equals(User.Role, role);
        }

        public UserPrincipal(UserForDetailed user)
        {
            User = user;
            Identity = new UserIdentity(user);
        }
    }
}