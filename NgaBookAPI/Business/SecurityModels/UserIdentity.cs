﻿using Business.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace Business.SecurityModels
{
    public class UserIdentity : IIdentity
    {
        public string Name => User.Username;

        public string AuthenticationType => "Jwt";

        public bool IsAuthenticated => true;

        public UserForDetailed User { get; }

        public UserIdentity(UserForDetailed user)
        {
            User = user;
        }
    }
}