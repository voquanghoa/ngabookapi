﻿using AutoMapper;
using Business.Dto;
using Business.Service;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Business.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<Author, AuthorForDetailed>();

            CreateMap<Category, CategoryForDetailed>();

            CreateMap<Book, BookForDetailed>();

            CreateMap<Author, AuthorForReferenced>();

            CreateMap<Category, CategoryForReferenced>();

            CreateMap<Book, BookForReferenced>();

            CreateMap<AuthorForInsert, Author>()
                .ForMember(dest => dest.Cover,
                    opt => opt.MapFrom(src => ImageService.SaveImage(Convert.FromBase64String(src.Cover))));

            CreateMap<CategoryForInsert, Category>();

            CreateMap<BookForInsert, Book>()
                .ForMember(dest => dest.Cover,
                    opt => opt.MapFrom(src => ImageService.SaveImage(Convert.FromBase64String(src.Cover))))
                .ForMember(dest => dest.Author,
                    opt => opt.Ignore())
                .ForMember(dest => dest.Categories,
                    opt => opt.Ignore());

            CreateMap<User, UserForDetailed>();

            CreateMap<UserForInsert, User>()
                .ForMember(dest => dest.Password,
                    opt => opt.Ignore())
                .ForMember(dest => dest.Role,
                    opt => opt.Ignore());
        }
    }
}