﻿using Business.Service;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Business.Utils
{
    public class AuthUtils
    {
        private static readonly string SecretKey = "t,jv=?Yd!%Dip:6/;;EbHFksfhlAWMnfsd&gt;~bl4}n`jad.5Aj";

        public static bool VerifyPassword(string password, string hashedPassword)
        {
            using (var md5 = new MD5CryptoServiceProvider())
            {
                var computedHash = Convert.ToBase64String(md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(password)));

                if (string.Equals(computedHash, hashedPassword))
                {
                    return true;
                }
            }
            return false;
        }

        public static string GenerateToken(int userId, string username, string role)
        {
            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, userId.ToString()),
                new Claim(ClaimTypes.Name, username),
                new Claim(ClaimTypes.Role, role)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(SecretKey));

            var cred = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = cred
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }

        public static bool ValidateToken(string token, out string userId, out string username, out string role)
        {
            userId = null;
            username = null;
            role = null;

            var simplePrincipal = GetPrincipal(token);
            var identity = simplePrincipal?.Identity as ClaimsIdentity;

            if (identity == null)
            {
                return false;
            }

            if (!identity.IsAuthenticated)
            {
                return false;
            }

            var userIdClaim = identity.FindFirst(ClaimTypes.NameIdentifier);
            userId = userIdClaim?.Value;

            var usernameClaim = identity.FindFirst(ClaimTypes.Name);
            username = usernameClaim?.Value;

            var roleClaim = identity.FindFirst(ClaimTypes.Role);
            role = roleClaim?.Value;

            if (string.IsNullOrEmpty(userId) || string.IsNullOrEmpty(username) || string.IsNullOrEmpty(role))
            {
                return false;
            }

            return true;
        }

        private static ClaimsPrincipal GetPrincipal(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var jwtToken = tokenHandler.ReadToken(token) as JwtSecurityToken;

            if (jwtToken == null)
            {
                return null;
            }

            var symmetricKey = Encoding.ASCII.GetBytes(SecretKey);

            var validationParameters = new TokenValidationParameters()
            {
                RequireExpirationTime = true,
                ValidateIssuer = false,
                ValidateAudience = false,
                IssuerSigningKey = new SymmetricSecurityKey(symmetricKey)
            };

            var principal = tokenHandler.ValidateToken(token, validationParameters, out _);

            return principal;
        }
    }
}