﻿using Business.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Business
{
    public interface IAuthsBusiness
    {
        UserToken Login(UserForLogin userForLogin);

        UserForDetailed GetUser(string username);
    }
}
