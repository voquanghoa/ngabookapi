﻿using AutoMapper;
using Business.Dto;
using Business.Exceptions;
using Business.SecurityModels;
using Business.Service;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Business.Business
{
    public class BooksBusiness : IBooksBusiness
    {
        private readonly IBookService _service;
        private readonly IMapper _mapper;

        public BooksBusiness(IBookService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        public IEnumerable<BookForDetailed> GetBooks()
        {
            var books = _service.GetAll();
            return _mapper.Map<IEnumerable<BookForDetailed>>(books);
        }

        public BookForDetailed GetBook(int id)
        {
            var book = _service.GetById(id)
                ?? throw new BadRequestException("Book Not Found");

            return _mapper.Map<BookForDetailed>(book);
        }

        public BookForDetailed AddBook(UserForDetailed owner, BookForInsert book)
        {
            var bookToAdd = _mapper.Map<Book>(book);

            _service.MapAuthorAndCategory(book, bookToAdd);

            bookToAdd.OwnerId = owner.Id;

            _service.Add(bookToAdd);

            return _mapper.Map<BookForDetailed>(bookToAdd);
        }

        public BookForDetailed UpdateBook(int id, UserForDetailed owner, BookForInsert book)
        {
            var role = (Role)Enum.Parse(typeof(Role), owner.Role, true);

            var bookToUpdate = _service.GetById(id)
                ?? throw new BadRequestException("Book Not Found");

            if (role == Role.EDITOR && bookToUpdate.OwnerId != owner.Id)
                throw new ForbiddenException("Access is denied for this resource");

            var updatedBook = _service.UpdateBook(id, book);

            return _mapper.Map<BookForDetailed>(updatedBook);
        }

        public void DeleteBook(UserForDetailed owner, int id)
        {
            var role = (Role)Enum.Parse(typeof(Role), owner.Role, true);

            var book = _service.GetById(id)
                ?? throw new BadRequestException("Book Not Found");

            if (role == Role.EDITOR && book.OwnerId != owner.Id)
                throw new ForbiddenException("Access is denied for this resource");

            _service.Delete(book);
        }

        public UserForDetailed GetUser(string username)
        {
            return _service.GetUser(username);
        }
    }
}