﻿using Business.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Business
{
    public interface IUsersBusiness
    {
        IEnumerable<UserForDetailed> GetUsers();

        UserForDetailed GetUser(int id);

        UserForDetailed AddUser(UserForInsert user);

        UserForDetailed UpdateUser(int id, UserForInsert user);

        void DeleteUser(int id);
    }
}
