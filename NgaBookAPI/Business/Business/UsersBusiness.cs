﻿using AutoMapper;
using Business.Dto;
using Business.Exceptions;
using Business.Service;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Business.Business
{
    public class UsersBusiness : IUsersBusiness
    {
        private readonly IUserService _service;
        private readonly IMapper _mapper;

        public UsersBusiness(IUserService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        public IEnumerable<UserForDetailed> GetUsers()
        {
            var users = _service.GetAll();
            return _mapper.Map<IEnumerable<UserForDetailed>>(users);
        }

        public UserForDetailed GetUser(int id)
        {
            var user = _service.GetById(id)
                ?? throw new BadRequestException("User Not Found");

            return _mapper.Map<UserForDetailed>(user);
        }

        public UserForDetailed AddUser(UserForInsert user)
        {
            if(_service.UsernameExists(user.Username))
            {
                throw new BadRequestException("Username Already Exists");
            }

            var userToAdd = _service.AddUser(user);

            return _mapper.Map<UserForDetailed>(userToAdd);
        }

        public UserForDetailed UpdateUser(int id, UserForInsert user)
        {
            if(!_service.Exists(id))
            {
                throw new BadRequestException("User Not Found");
            }

            if(!string.Equals(_service.GetById(id).Username, user.Username))
            {
                throw new BadRequestException("Cannot update username");
            }

            var userToUpdate = _service.Update(id, user);
            return _mapper.Map<UserForDetailed>(userToUpdate);
        }

        public void DeleteUser(int id)
        {
            var user = _service.GetById(id)
                ?? throw new BadRequestException("User Not Found");

            _service.Delete(user);
        }
    }
}