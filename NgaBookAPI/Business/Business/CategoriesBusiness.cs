﻿using AutoMapper;
using Business.Dto;
using Business.Exceptions;
using Business.Service;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Business.Business
{
    public class CategoriesBusiness : ICategoriesBusiness
    {
        private readonly ICategoryService _service;
        private readonly IMapper _mapper;

        public CategoriesBusiness(ICategoryService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        public IEnumerable<CategoryForDetailed> GetCategories()
        {
            var categories = _service.GetAll();
            return _mapper.Map<IEnumerable<CategoryForDetailed>>(categories);
        }

        public CategoryForDetailed GetCategory(int id)
        {
            var category = _service.GetById(id)
                ?? throw new BadRequestException("Category Not Found");

            return _mapper.Map<CategoryForDetailed>(category);
        }

        public CategoryForDetailed AddCategory(CategoryForInsert category)
        {
            var categoryToAdd = _mapper.Map<Category>(category);
            _service.Add(categoryToAdd);

            return _mapper.Map<CategoryForDetailed>(categoryToAdd);
        }

        public CategoryForDetailed UpdateCategory(int id, CategoryForInsert category)
        {
            if (!_service.Exists(id))
            {
                throw new BadRequestException("Category Not Found");
            }

            var categoryToUpdate = _service.Update(id, category);

            return _mapper.Map<CategoryForDetailed>(categoryToUpdate);         
        }

        public void DeleteCategory(int id)
        {
            var category = _service.GetById(id)
                ?? throw new BadRequestException("Category Not Found");

            _service.Delete(category);
        }
    }
}