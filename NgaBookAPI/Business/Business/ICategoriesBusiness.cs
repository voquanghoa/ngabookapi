﻿using Business.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Business
{
    public interface ICategoriesBusiness
    {
        IEnumerable<CategoryForDetailed> GetCategories();

        CategoryForDetailed GetCategory(int id);

        CategoryForDetailed AddCategory(CategoryForInsert category);

        CategoryForDetailed UpdateCategory(int id, CategoryForInsert category);

        void DeleteCategory(int id);
    }
}
