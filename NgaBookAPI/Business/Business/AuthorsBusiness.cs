﻿using AutoMapper;
using Business.Dto;
using Business.Exceptions;
using Business.Service;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Business.Business
{
    public class AuthorsBusiness : IAuthorsBusiness
    {
        private readonly IAuthorService _service;
        private readonly IMapper _mapper;

        public AuthorsBusiness(IAuthorService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        public IEnumerable<AuthorForDetailed> GetAuthors()
        {
            var authors = _service.GetAll();
            return _mapper.Map<IEnumerable<AuthorForDetailed>>(authors);
        }

        public AuthorForDetailed GetAuthor(int id)
        {
            var author = _service.GetById(id) 
                ?? throw new BadRequestException("Author Not Found");

            return _mapper.Map<AuthorForDetailed>(author);
        }

        public AuthorForDetailed AddAuthor(AuthorForInsert author)
        {
            var authorToAdd = _mapper.Map<Author>(author);
            _service.Add(authorToAdd);

            return _mapper.Map<AuthorForDetailed>(authorToAdd);
        }

        public AuthorForDetailed UpdateAuthor(int id, AuthorForInsert author)
        {
            if (!_service.Exists(id))
            {
                throw new BadRequestException("Author Not Found");
            }

            var authorToUpdate = _service.Update(id, author);

            return _mapper.Map<AuthorForDetailed>(authorToUpdate);
        }

        public void DeleteAuthor(int id)
        {
            var author = _service.GetById(id) 
                ?? throw new BadRequestException("Author Not Found");

            _service.Delete(author);
        }
    }
}