﻿using Business.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Business.Business
{
    public interface IAuthorsBusiness
    {
        IEnumerable<AuthorForDetailed> GetAuthors();

        AuthorForDetailed GetAuthor(int id);

        AuthorForDetailed AddAuthor(AuthorForInsert author);

        AuthorForDetailed UpdateAuthor(int id, AuthorForInsert author);

        void DeleteAuthor(int id);
    }
}