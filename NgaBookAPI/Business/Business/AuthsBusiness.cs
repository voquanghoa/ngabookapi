﻿using Business.Dto;
using Business.Service;
using Business.Utils;

namespace Business.Business
{
    public class AuthsBusiness : IAuthsBusiness
    {
        private readonly IAuthService _service;

        public AuthsBusiness(IAuthService service)
        {
            _service = service;
        }

        public UserToken Login(UserForLogin userForLogin)
        {
            var user = _service.Login(userForLogin);

            return new UserToken(AuthUtils.GenerateToken(user.Id, user.Username, user.Role), user.Name);
        }

        public UserForDetailed GetUser(string username)
        {
            return _service.GetUser(username);
        }
    }
}