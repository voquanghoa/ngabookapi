﻿using Business.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Business
{
    public interface IBooksBusiness
    {
        IEnumerable<BookForDetailed> GetBooks();

        BookForDetailed GetBook(int id);

        BookForDetailed AddBook(UserForDetailed owner, BookForInsert book);

        BookForDetailed UpdateBook(int id, UserForDetailed owner, BookForInsert book);

        void DeleteBook(UserForDetailed owner, int id);

        UserForDetailed GetUser(string username);
    }
}
