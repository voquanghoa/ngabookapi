﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Business.Dto
{
    public class AuthorForDetailed
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Website { get; set; }

        public DateTime? Birthday { get; set; }

        public string Cover { get; set; }

        public virtual ICollection<BookForReferenced> Books { get; set; }
    }
}