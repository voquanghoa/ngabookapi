﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Business.Dto
{
    public class UserForLogin
    {
        public string Username { get; set; }

        public string Password { get; set; }
    }
}