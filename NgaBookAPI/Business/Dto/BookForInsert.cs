﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Business.Dto
{
    public class BookForInsert
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public double Price { get; set; }

        public int Year { get; set; }

        public virtual AuthorForReferenced Author { get; set; }

        public string Publisher { get; set; }

        public string Cover { get; set; }

        public virtual ICollection<CategoryForReferenced> Categories { get; set; }
    }
}