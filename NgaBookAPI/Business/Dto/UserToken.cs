﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Business.Dto
{
    public class UserToken
    {
        public UserToken(string token, string userName)
        {
            Token = token;
            User = userName;
        }

        public string Token { get; set; }

        public string User { get; set; }
    }
}