﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Business.Dto
{
    public class CategoryForDetailed
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public virtual ICollection<BookForReferenced> Books { get; set; }
    }
}