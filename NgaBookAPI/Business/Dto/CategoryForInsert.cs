﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Business.Dto
{
    public class CategoryForInsert
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}