﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace NgaBookAPI.Filters
{
    public class ValidateModelAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (actionContext.ModelState.IsValid == false)
            {
                var message = actionContext.ModelState.Values.FirstOrDefault().Errors[0].ErrorMessage;
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest, message);
            }
        }

    }
}