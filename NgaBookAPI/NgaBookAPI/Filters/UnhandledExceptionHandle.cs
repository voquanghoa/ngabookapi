﻿using Business.Exceptions;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Filters;

namespace NgaBookAPI.Filters
{
    public class UnhandledExceptionHandle : ActionFilterAttribute
    {
        public override void OnActionExecuted(HttpActionExecutedContext context)
        {
            var exception = context.Exception;

            if (exception != null && !(exception is BaseException))
            {
                LogManager.GetLogger("API Logger").Error(exception);
            }
        }
    }

}