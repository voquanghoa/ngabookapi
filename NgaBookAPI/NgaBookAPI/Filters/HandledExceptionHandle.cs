﻿using Business.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Filters;

namespace NgaBookAPI.Filters
{
    public class HandledExceptionHandle : ActionFilterAttribute
    {
        public override void OnActionExecuted(HttpActionExecutedContext context)
        {
            if (context.Exception is BaseException exception)
            {
                context.Response = exception.CreateResponse(context.Request);
            }
        }
    }
}