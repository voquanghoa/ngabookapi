﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Business.Dto;
using Business.Business;

namespace NgaBookAPI.Controllers
{
    [RoutePrefix("api/authors")]
    public class AuthorsController : ApiController
    {
        private readonly IAuthorsBusiness _business;
        public AuthorsController(IAuthorsBusiness business)
        {
            _business = business;
        }

        [HttpGet]
        public IEnumerable<AuthorForDetailed> GetAuthors() => _business.GetAuthors();

        [HttpGet]
        [Route("{id}")]
        public AuthorForDetailed GetAuthor(int id) => _business.GetAuthor(id);

        [HttpPost]
        public AuthorForDetailed AddAuthor(AuthorForInsert author) => _business.AddAuthor(author);

        [HttpPut]
        [Route("{id}")]
        public AuthorForDetailed UpdateAuthor(int id, AuthorForInsert author) => _business.UpdateAuthor(id, author);

        [HttpDelete]
        [Route("{id}")]
        public void DeleteAuthor(int id) => _business.DeleteAuthor(id);

    }
}