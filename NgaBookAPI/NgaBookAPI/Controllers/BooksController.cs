﻿using Business.Business;
using Business.Dto;
using Business.SecurityModels;
using Business.Utils;
using Models.Models;
using NgaBookAPI.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;

namespace NgaBookAPI.Controllers
{
    [RoutePrefix("api/books")]
    public class BooksController : ApiController
    {
        private readonly IBooksBusiness _business;
        private UserForDetailed Owner
        { 
            get
            {
                return (HttpContext.Current.User.Identity as UserIdentity).User;
            }
        }

        public BooksController(IBooksBusiness business)
        {
            _business = business;
        }

        [HttpGet]
        public IEnumerable<BookForDetailed> GetBooks() => _business.GetBooks();

        [HttpGet]
        [Route("{id}")]
        public BookForDetailed GetBook(int id) => _business.GetBook(id);

        [HttpPost]
        [Require(Role.EDITOR)]
        public BookForDetailed AddBook(BookForInsert book) => _business.AddBook(Owner, book);

        [HttpPut]
        [Route("{id}")]
        [Require(Role.EDITOR)]
        public BookForDetailed UpdateBook(int id, BookForInsert book) => _business.UpdateBook(id, Owner, book);

        [HttpDelete]
        [Route("{id}")]
        [Require(Role.EDITOR)]
        public void DeleteBook(int id) => _business.DeleteBook(Owner, id);
    }
}
