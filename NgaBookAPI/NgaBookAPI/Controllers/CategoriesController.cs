﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Business.Business;
using Business.Dto;

namespace NgaBookAPI.Controllers
{
    [RoutePrefix("api/categories")]
    public class CategoriesController : ApiController
    {
        private readonly ICategoriesBusiness _business;

        public CategoriesController(ICategoriesBusiness business)
        {
            _business = business;
        }

        [HttpGet]
        public IEnumerable<CategoryForDetailed> GetCategories() => _business.GetCategories();

        [HttpGet]
        [Route("{id}")]
        public CategoryForDetailed GetCategory(int id) => _business.GetCategory(id);

        [HttpPost]
        public CategoryForDetailed AddCategory(CategoryForInsert category) => _business.AddCategory(category);

        [HttpPut]
        [Route("{id}")]
        public CategoryForDetailed UpdateCategory(int id, CategoryForInsert category) => _business.UpdateCategory(id, category);

        [HttpDelete]
        [Route("{id}")]
        public void DeleteCategory(int id) => _business.DeleteCategory(id);
    }
}