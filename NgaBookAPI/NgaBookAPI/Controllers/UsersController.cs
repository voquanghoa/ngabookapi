﻿using Business.Business;
using Business.Dto;
using Models.Models;
using NgaBookAPI.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NgaBookAPI.Controllers
{
    [RoutePrefix("api/users")]
    [Require(Role.MANAGER)]
    public class UsersController : ApiController
    {
        private readonly IUsersBusiness _business;

        public UsersController(IUsersBusiness business)
        {
            _business = business;
        }

        [HttpGet]
        public IEnumerable<UserForDetailed> GetUsers() => _business.GetUsers();

        [HttpGet]
        [Route("{id}")]
        public UserForDetailed GetUser(int id) => _business.GetUser(id);

        [HttpPost]
        public UserForDetailed AddUser(UserForInsert user) => _business.AddUser(user);

        [HttpPut]
        [Route("{id}")]
        public UserForDetailed UpdateUser(int id, UserForInsert user) => _business.UpdateUser(id, user);


        [HttpDelete]
        [Route("{id}")]
        public void DeleteUser(int id) => _business.DeleteUser(id);
    }
}
