﻿using Business.Business;
using Business.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NgaBookAPI.Controllers
{
    [RoutePrefix("api/auths")]
    public class AuthsController : ApiController
    {
        private readonly IAuthsBusiness _business;

        public AuthsController(IAuthsBusiness business)
        {
            _business = business;
        }

        [HttpPost]
        [Route("login")]
        [AllowAnonymous]
        public UserToken Login(UserForLogin user) => _business.Login(user);
    }
}
