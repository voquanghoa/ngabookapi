﻿using AutoMapper;
using Business.Business;
using Business.Helpers;
using Business.Service;
using Models;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Unity;
using EF.DbContextFactory.Unity.Extensions;
using Unity.Injection;

namespace NgaBookAPI.DIConfig
{
    public class UnityConfiguration
    {
        public static IUnityContainer Config()
        {
            var container = new UnityContainer();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperProfiles());
            });

            container.RegisterInstance(config.CreateMapper());

            container.RegisterFactory<ApplicationDbContext>(x => new ApplicationDbContextFactory().Create());

            container.RegisterType<IAuthorService, AuthorService>();
            container.RegisterType<ICategoryService, CategoryService>();
            container.RegisterType<IBookService, BookService>();
            container.RegisterType<IUserService, UserService>();
            container.RegisterType<IAuthService, AuthService>();

            container.RegisterType<IAuthsBusiness, AuthsBusiness>();
            container.RegisterType<IAuthorsBusiness, AuthorsBusiness>();
            container.RegisterType<ICategoriesBusiness, CategoriesBusiness>();
            container.RegisterType<IBooksBusiness, BooksBusiness>();
            container.RegisterType<IUsersBusiness, UsersBusiness>();

            return container;
        }
    }
}