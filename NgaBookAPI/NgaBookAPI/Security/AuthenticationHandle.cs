﻿using Business.Business;
using Business.SecurityModels;
using Business.Utils;
using System;
using System.Net;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;

namespace NgaBookAPI.Security
{
    public class AuthenticationHandle : Attribute, IAuthenticationFilter
    {
        public string Realm { get; set; }
        public bool AllowMultiple => false;

        private IAuthsBusiness _business;

        public AuthenticationHandle(IAuthsBusiness business)
        {
            _business = business;
        }

        public async Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            var request = context.Request;
            var authorization = request.Headers.Authorization;

            if (authorization == null || authorization.Scheme != "Bearer")
            {
                return;
            }

            if (string.IsNullOrEmpty(authorization.Parameter))
            {
                context.ErrorResult = new HttpResult(HttpStatusCode.Unauthorized, "Missing Jwt Token", request);
                return;
            }

            var token = authorization.Parameter;
            var principal = await AuthenticateJwtToken(token);

            if (principal == null)
            {
                context.ErrorResult = new HttpResult(HttpStatusCode.Unauthorized, "Invalid token", request);
            }

            else
            {
                context.Principal = principal;
            }
        }

        protected Task<IPrincipal> AuthenticateJwtToken(string token)
        {
            if (AuthUtils.ValidateToken(token, out var userId, out var username, out var role))
            {
                var identity = CreateIIdentity(username);
                IPrincipal user = new UserPrincipal(identity.User);

                return Task.FromResult(user);
            }

            return Task.FromResult<IPrincipal>(null);
        }

        private UserIdentity CreateIIdentity(string username)
        {
            return new UserIdentity(_business.GetUser(username));
        }

        public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            Challenge(context);
            return Task.FromResult(0);
        }

        private void Challenge(HttpAuthenticationChallengeContext context)
        {
            string parameter = null;

            if (!string.IsNullOrEmpty(Realm))
                parameter = "realm=\"" + Realm + "\"";

            context.ChallengeWith("Bearer", parameter);
        }
    }
}
