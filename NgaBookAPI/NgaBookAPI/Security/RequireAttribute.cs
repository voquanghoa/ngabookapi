﻿using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Filters;
using Business.SecurityModels;
using System.Threading;
using System.Threading.Tasks;
using System.Net;

namespace NgaBookAPI.Security
{
    public class RequireAttribute : Attribute, IAuthenticationFilter
    {
        public bool AllowMultiple => false;

        public Role Role { get; set; }

        public RequireAttribute(Role role)
        {
            this.Role = role;
        }

        public async Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            if (context.Principal is UserPrincipal claimsPrincipal)
            {
                if (claimsPrincipal.Identity is UserIdentity userIdentity)
                {
                    if ((Role)Enum.Parse(typeof(Role), userIdentity.User.Role, true) <= Role)
                    {
                        return;
                    }
                }
            }

            context.ErrorResult = new HttpResult(HttpStatusCode.Forbidden, "You don't have permission to perform this task", context.Request);

            await Task.FromResult<object>(null);
        }

        public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            return Task.FromResult(0);
        }
    }
}