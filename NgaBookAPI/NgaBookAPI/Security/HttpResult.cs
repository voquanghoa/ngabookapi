﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace NgaBookAPI.Security
{
    public class HttpResult : IHttpActionResult
    {

        public HttpStatusCode StatusCode { get; set; }

        public string Message { get; }

        public HttpRequestMessage Request { get; }

        public HttpResult(HttpStatusCode statusCode, string message, HttpRequestMessage request)
        {
            StatusCode = statusCode;
            Message = message;
            Request = request;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            return Task.FromResult(Execute());
        }

        private HttpResponseMessage Execute()
        {
            HttpResponseMessage response = new HttpResponseMessage()
            {
                StatusCode = StatusCode,
                RequestMessage = Request,
                Content = new StringContent(JsonConvert.SerializeObject(new
                {
                    Message
                }))
            };

            return response;
        }
    }
}
