﻿using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;

namespace Models.Data
{
    public class DataFactory
    {
        public List<Category> Categories { get; set; }
        public List<Author> Authors { get; set; }
        public List<Book> Books { get; set; }

        public DataFactory()
        {
            var authorData = File.ReadAllText("Data/AuthorData.json");
            var categoryData = File.ReadAllText("Data/CategoryData.json");
            var bookData = File.ReadAllText("Data/BookData.json");

            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = "dd-MM-YYYY" };

            Categories = JsonConvert.DeserializeObject<List<Category>>(categoryData);
            Authors = JsonConvert.DeserializeObject<List<Author>>(authorData, dateTimeConverter);
            Books = JsonConvert.DeserializeObject<List<Book>>(bookData);
        }
    }
}