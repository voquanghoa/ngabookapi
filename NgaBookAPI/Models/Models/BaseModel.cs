﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Models.Models
{
    public class BaseModel
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }
    }
}