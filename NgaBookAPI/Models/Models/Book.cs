﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Models.Models
{
    public class Book : BaseModel
    {
        public Book()
        {
            Categories = new HashSet<Category>();
        }

        public string Description { get; set; }

        public double Price { get; set; }

        public int Year { get; set; }

        public int? AuthorId { get; set; }

        [ForeignKey("AuthorId")]
        public virtual Author Author { get; set; }

        public string Publisher { get; set; }

        public string Cover { get; set; }

        public virtual ICollection<Category> Categories { get; set; }

        public int? OwnerId { get; set; }

        [ForeignKey("OwnerId")]
        public virtual User Owner { get; set; }
    }
}