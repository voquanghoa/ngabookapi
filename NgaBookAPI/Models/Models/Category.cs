﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Models.Models
{
    public class Category : BaseModel
    {
        public Category()
        {
            Books = new HashSet<Book>();    
        }

        public string Description { get; set; }

        public virtual ICollection<Book> Books { get; set; }
    }
}