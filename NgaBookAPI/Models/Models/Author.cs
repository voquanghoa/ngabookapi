﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Models.Models
{
    public class Author : BaseModel
    {
        public string Website { get; set; }

        public DateTime? Birthday { get; set; }

        public string Cover { get; set; }

        public virtual ICollection<Book> Books { get; set; }
    }
}