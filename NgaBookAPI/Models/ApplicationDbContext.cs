﻿using Models.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(string connStr) : base(connStr)
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>()
                        .HasMany<Book>(c => c.Books)
                        .WithMany(b => b.Categories)
                        .Map(cb =>
                        {
                            cb.MapLeftKey("CategoryId");
                            cb.MapRightKey("BookId");
                            cb.ToTable("CategoryBook");
                        });
        }

        public DbSet<Book> Books { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Author> Authors { get; set; }

        public DbSet<User> Users { get; set; }
    }
}