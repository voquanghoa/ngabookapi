﻿namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditDB : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.CategoryBooks", newName: "CategoryBook");
            RenameColumn(table: "dbo.Books", name: "Author_Id", newName: "AuthorId");
            RenameColumn(table: "dbo.CategoryBook", name: "Category_Id", newName: "CategoryId");
            RenameColumn(table: "dbo.CategoryBook", name: "Book_Id", newName: "BookId");
            RenameIndex(table: "dbo.Books", name: "IX_Author_Id", newName: "IX_AuthorId");
            RenameIndex(table: "dbo.CategoryBook", name: "IX_Category_Id", newName: "IX_CategoryId");
            RenameIndex(table: "dbo.CategoryBook", name: "IX_Book_Id", newName: "IX_BookId");
            AlterColumn("dbo.Authors", "Birthday", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Authors", "Birthday", c => c.DateTime(nullable: false));
            RenameIndex(table: "dbo.CategoryBook", name: "IX_BookId", newName: "IX_Book_Id");
            RenameIndex(table: "dbo.CategoryBook", name: "IX_CategoryId", newName: "IX_Category_Id");
            RenameIndex(table: "dbo.Books", name: "IX_AuthorId", newName: "IX_Author_Id");
            RenameColumn(table: "dbo.CategoryBook", name: "BookId", newName: "Book_Id");
            RenameColumn(table: "dbo.CategoryBook", name: "CategoryId", newName: "Category_Id");
            RenameColumn(table: "dbo.Books", name: "AuthorId", newName: "Author_Id");
            RenameTable(name: "dbo.CategoryBook", newName: "CategoryBooks");
        }
    }
}
