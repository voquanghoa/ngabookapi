﻿namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditBooksTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Books", "OwnerId", c => c.Int());
            CreateIndex("dbo.Books", "OwnerId");
            AddForeignKey("dbo.Books", "OwnerId", "dbo.Users", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Books", "OwnerId", "dbo.Users");
            DropIndex("dbo.Books", new[] { "OwnerId" });
            DropColumn("dbo.Books", "OwnerId");
        }
    }
}
