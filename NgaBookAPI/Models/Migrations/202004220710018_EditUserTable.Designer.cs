﻿// <auto-generated />
namespace Models.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.0")]
    public sealed partial class EditUserTable : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(EditUserTable));
        
        string IMigrationMetadata.Id
        {
            get { return "202004220710018_EditUserTable"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
