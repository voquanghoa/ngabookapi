﻿namespace Models.Migrations
{
    using global::Models.Data;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<global::Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(global::Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.
            DataFactory dataFactory = new DataFactory();

            context.Categories.AddRange(dataFactory.Categories);
            context.Authors.AddRange(dataFactory.Authors);
            context.Books.AddRange(dataFactory.Books);
        }
    }
}
