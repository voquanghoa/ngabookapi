﻿using Business.Business;
using NgaBookAPI.DIConfig;
using NgaBookAPI.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace NgaBookAPI.Test
{
    public static class TestConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var container = UnityConfiguration.Config();
            config.DependencyResolver = new UnityResolver(container);

            // Web API configuration and services
            config.Formatters.Clear();

            var jsonFormatter = new JsonMediaTypeFormatter();
            jsonFormatter.SupportedMediaTypes.Clear();
            jsonFormatter.SupportedMediaTypes.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            config.Formatters.Add(jsonFormatter);

            config.Filters.Add(new AuthenticationHandle((IAuthsBusiness)container.Resolve(typeof(IAuthsBusiness), "Auth")));
            config.Filters.Add(new AuthorizeAttribute());

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
