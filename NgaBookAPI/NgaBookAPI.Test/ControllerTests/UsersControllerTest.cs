﻿using Business.Business;
using Business.Dto;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NgaBookAPI.Controllers;
using NgaBookAPI.DIConfig;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;

namespace NgaBookAPI.Test.ControllerTests
{
    [TestClass]
    public class UsersControllerTest
    {
        private readonly IUnityContainer container;
        private readonly UsersController controller;

        public UsersControllerTest()
        {
            container = UnityConfiguration.Config();
            controller = new UsersController(container.Resolve<UsersBusiness>());
        }

        [TestMethod]
        public void Test_GetUsers()
        {
            var result = controller.GetUsers();

            Assert.AreEqual(result.Count(), 5);
        }

        [TestMethod]
        public void Test_GetUser()
        {
            var result = controller.GetUser(11);

            Assert.AreEqual(result.Username, "user2");
            Assert.AreEqual(result.Role, "USER");
        }

        [TestMethod]
        public void Test_AddUser()
        {
            var user = new UserForInsert()
            {
                Name = "Manager 2",
                Username = "manager2",
                Password = "password",
                Role = "MANAGER"
            };

            var result = controller.AddUser(user);

            Assert.AreEqual(result.Username, user.Username);
            Assert.AreEqual(result.Role, user.Role);
        }

        [TestMethod]
        public void Test_UpdateUser()
        {
            var user = new UserForInsert()
            {
                Name = "Manager 222",
                Username = "manager2",
                Password = "password",
                Role = "MANAGER"
            };

            var result = controller.UpdateUser(12, user);

            Assert.AreEqual(result.Name, user.Name);
        }

        [TestMethod]
        public void Test_DeleteUser()
        {
            var count = controller.GetUsers().Count();

            controller.DeleteUser(12);

            Assert.AreEqual(controller.GetUsers().Count(), count - 1);
        }
    }
}
