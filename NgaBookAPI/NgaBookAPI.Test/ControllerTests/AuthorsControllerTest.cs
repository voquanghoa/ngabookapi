﻿using Business.Business;
using Business.Dto;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using NgaBookAPI.Controllers;
using NgaBookAPI.DIConfig;
using NgaBookAPI.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Unity;

namespace NgaBookAPI.Test.ControllerTests
{
    [TestClass]
    public class AuthorsControllerTest
    {
        private readonly IUnityContainer container;
        private readonly AuthorsController controller;

        private readonly HttpConfiguration config;

        public AuthorsControllerTest()
        {
            container = UnityConfiguration.Config();
            controller = new AuthorsController(container.Resolve<AuthorsBusiness>());

            config = new HttpConfiguration();
            TestConfig.Register(config);
        }

        [TestMethod]
        public async Task Test_GetAuthors()
        {
            using (var server = new HttpServer(config))
            {
                var client = GenerateClient(server);

                string url = "https://localhost/api/authors/";

                var request = new HttpRequestMessage
                {
                    RequestUri = new Uri(url),
                    Method = HttpMethod.Get
                };

                using (var response = await client.SendAsync(request))
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

                    string responseBody = await response.Content.ReadAsStringAsync();

                    var result = JsonConvert.DeserializeObject(responseBody, typeof(IEnumerable<AuthorForDetailed>)) as IEnumerable<AuthorForDetailed>;
                    Assert.AreEqual(result.Count(), controller.GetAuthors().Count());
                }
            }
        }

        [TestMethod]
        public async Task Test_GetAuthor()
        {
            using (var server = new HttpServer(config))
            {
                var client = GenerateClient(server);

                string url = "https://localhost/api/authors/12";

                var request = new HttpRequestMessage
                {
                    RequestUri = new Uri(url),
                    Method = HttpMethod.Get
                };

                using (var response = await client.SendAsync(request))
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

                    string responseBody = await response.Content.ReadAsStringAsync();

                    var result = JsonConvert.DeserializeObject(responseBody, typeof(AuthorForDetailed)) as AuthorForDetailed;
                    Assert.AreEqual(result.Name, controller.GetAuthor(12).Name);
                }
            }
        }

        [TestMethod]
        public async Task Test_AddAuthor()
        {
            using (var server = new HttpServer(config))
            {
                var client = GenerateClient(server);

                string url = "https://localhost/api/authors/";
                string author = "{\"Name\": \"TestAdd\", \"Website\": null, \"Birthday\": \"1955-05-07T00:00:00\"}";

                var request = new HttpRequestMessage
                {
                    RequestUri = new Uri(url),
                    Method = HttpMethod.Post,
                    Content = new StringContent(author, Encoding.UTF8, "application/json")
                };

                using (var response = await client.SendAsync(request))
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

                    string responseBody = await response.Content.ReadAsStringAsync();

                    var result = JsonConvert.DeserializeObject(responseBody, typeof(AuthorForDetailed)) as AuthorForDetailed;
                    Assert.AreEqual(result.Name, "TestAdd");
                }
            }
        }

        [TestMethod]
        public async Task Test_UpdateAuthor()
        {
            using (var server = new HttpServer(config))
            {
                var client = GenerateClient(server);

                string url = "https://localhost/api/authors/13";
                string author = "{\"Name\": \"TestUpdate\", \"Website\": null, \"Birthday\": \"1955-05-07T00:00:00\"}";

                var request = new HttpRequestMessage
                {
                    RequestUri = new Uri(url),
                    Method = HttpMethod.Put,
                    Content = new StringContent(author, Encoding.UTF8, "application/json")
                };

                using (var response = await client.SendAsync(request))
                {
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

                    string responseBody = await response.Content.ReadAsStringAsync();

                    var result = JsonConvert.DeserializeObject(responseBody, typeof(AuthorForDetailed)) as AuthorForDetailed;
                    Assert.AreEqual(result.Name, "TestUpdate");
                }
            }
        }

        [TestMethod]
        public async Task Test_DeleteAuthor()
        {
            using (var server = new HttpServer(config))
            {
                var client = GenerateClient(server);

                string url = "https://localhost/api/authors/13";

                var request = new HttpRequestMessage
                {
                    RequestUri = new Uri(url),
                    Method = HttpMethod.Delete
                };

                using (var response = await client.SendAsync(request))
                {
                    Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
                }
            }
        }

        private HttpClient GenerateClient(HttpServer server)
        {
            var client = new HttpClient(server);
            client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJuYW1laWQiOiIxMCIsInVuaXF1ZV9uYW1lIjoiZWRpdG9yMSIsInJvbGUiOiJFRElUT1IiLCJuYmYiOjE1ODgwNTk3MjcsImV4cCI6MTU4ODE2NjEyNywiaWF0IjoxNTg4MDU5NzI3fQ.asCc22VpQPPKtnXbr4GNVtcslU6CZCKyrdzYjin8yZqIkOxi61enMhlCcL_ZnGUlH5j7AiBIzaJvAMkqZrcUog");

            return client;
        }
    }
}
