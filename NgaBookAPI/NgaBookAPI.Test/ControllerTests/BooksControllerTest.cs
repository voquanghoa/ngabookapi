﻿using Business.Business;
using Business.Dto;
using Business.SecurityModels;
using FakeHttpContext;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NgaBookAPI.Controllers;
using NgaBookAPI.DIConfig;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.UI;
using Unity;

namespace NgaBookAPI.Test.ControllerTests
{
    [TestClass]
    public class BooksControllerTest
    {
        private readonly IUnityContainer container;
        private readonly BooksController controller;

        public BooksControllerTest()
        {
            container = UnityConfiguration.Config();
            controller = new BooksController(container.Resolve<BooksBusiness>());
        }

        [TestMethod]
        public void Test_GetBooks()
        {
            var result = controller.GetBooks();

            Assert.AreEqual(result.Count(), 8);
        }

        [TestMethod]
        public void Test_GetBook()
        {
            var result = controller.GetBook(4);

            Assert.AreEqual(result.Name, "City of Bones");
            Assert.AreEqual(result.Year, 2013);
        }

        [TestMethod]
        public void Test_AddBook()
        {
            using (var context = new FakeHttpContext.FakeHttpContext())
            {
                HttpContext.Current.User = FakePrincipal();

                var book = new BookForInsert()
                {
                    Name = "TestAdd",
                    Description = "Test",
                    Price = 5.4,
                    Year = 2019,
                    Author = new AuthorForReferenced()
                    {
                        Id = 2,
                        Name = "Mark Twain",
                        Website = null,
                        Birthday = new DateTime(1835, 11, 30),
                        Cover = null
                    },
                    Publisher = "Test",
                    Categories = new CategoryForReferenced[] { }
                };

                var result = controller.AddBook(book);

                Assert.AreEqual(result.Name, book.Name);
            }
        }

        [TestMethod]
        public void Test_UpdateBook()
        {
            using (var context = new FakeHttpContext.FakeHttpContext())
            {
                HttpContext.Current.User = FakePrincipal();

                var book = new BookForInsert()
                {
                    Name = "TestUpdate",
                    Description = "Test",
                    Price = 5.4,
                    Year = 2019,
                    Author = new AuthorForReferenced()
                    {
                        Id = 2,
                        Name = "Mark Twain",
                        Website = null,
                        Birthday = new DateTime(1835, 11, 30),
                        Cover = null
                    },
                    Publisher = "Test",
                    Categories = new CategoryForReferenced[] { }
                };

                var result = controller.UpdateBook(8, book);

                Assert.AreEqual(result.Name, book.Name);
            }
        }

        [TestMethod]
        public void Test_DeleteBook()
        {
            var count = controller.GetBooks().Count();

            using (var context = new FakeHttpContext.FakeHttpContext())
            {
                HttpContext.Current.User = FakePrincipal();

                controller.DeleteBook(11);

                Assert.AreEqual(controller.GetBooks().Count(), count - 1);
            }
        }

        private UserPrincipal FakePrincipal()
        {
            var user = new UserForDetailed()
            {
                Id = 10,
                Name = "Editor 1",
                Username = "editor1",
                Role = "EDITOR"
            };

            return new UserPrincipal(user);
        }
    }
}
