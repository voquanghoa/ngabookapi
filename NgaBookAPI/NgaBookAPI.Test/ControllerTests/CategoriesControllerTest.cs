﻿using Business.Business;
using Business.Dto;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NgaBookAPI.Controllers;
using NgaBookAPI.DIConfig;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;

namespace NgaBookAPI.Test.ControllerTests
{
    [TestClass]
    public class CategoriesControllerTest
    {
        private readonly IUnityContainer container;
        private readonly CategoriesController controller;

        public CategoriesControllerTest()
        {
            container = UnityConfiguration.Config();
            controller = new CategoriesController(container.Resolve<CategoriesBusiness>());
        }

        [TestMethod]
        public void Test_GetCategories()
        {
            var result = controller.GetCategories();

            Assert.AreEqual(result.Count(), 7);
        }

        [TestMethod]
        public void Test_GetCategory()
        {
            var result = controller.GetCategory(5);

            Assert.AreEqual(result.Name, "Adventure");
            Assert.AreEqual(result.Books.ElementAt(0).Id, 2);
        }

        [TestMethod]
        public void Test_AddCategory()
        {
            var category = new CategoryForInsert()
            {
                Name = "TestAdd",
                Description = "TestAdd"
            };

            var result = controller.AddCategory(category);

            Assert.AreEqual(result.Name, category.Name);
            Assert.AreEqual(result.Description, category.Description);
        }

        [TestMethod]
        public void Test_UpdateCategory()
        {
            var category = new CategoryForInsert()
            { 
                Name = "TestUpdate",
                Description = "TestUpdate"
            };

            var result = controller.UpdateCategory(7, category);

            Assert.AreEqual(result.Name, category.Name);
            Assert.AreEqual(result.Description, category.Description);            
        }

        [TestMethod]
        public void Test_DeleteCategory()
        {
            var count = controller.GetCategories().Count();

            controller.DeleteCategory(8);

            Assert.AreEqual(controller.GetCategories().Count(), count - 1);
        }
    }
}
