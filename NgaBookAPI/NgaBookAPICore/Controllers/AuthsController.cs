﻿using Business.Business;
using Business.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace NgaBookAPICore.Controllers
{
    [Route("api/auths")]
    [ApiController]
    public class AuthsController : ControllerBase
    {
        private readonly IAuthsBusiness _business;

        public AuthsController(IAuthsBusiness business)
        {
            _business = business;
        }

        [HttpPost("login")]
        [AllowAnonymous]
        public UserToken Login(UserForLogin user) => _business.Login(user);
    }
}
