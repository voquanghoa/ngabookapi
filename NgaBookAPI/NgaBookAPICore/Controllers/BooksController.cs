﻿using Business.Business;
using Business.Dto;
using Business.SecurityModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models.Models;
using System;
using System.Collections.Generic;

namespace NgaBookAPICore.Controllers
{
    [Route("api/books")]
    [ApiController]
    [Authorize]
    public class BooksController : ControllerBase
    {
        private readonly IBooksBusiness _business;
        private UserForDetailed Owner
        {
            get
            {
                var username = HttpContext.User.Identity.Name;
                return _business.GetUser(username);
            }
        }

        public BooksController(IBooksBusiness business)
        {
            _business = business;
        }

        [HttpGet]
        public IEnumerable<BookForDetailed> GetBooks() => _business.GetBooks();

        [HttpGet("{id}")]
        public BookForDetailed GetBook(int id) => _business.GetBook(id);

        [HttpPost]
        [Authorize(Roles = "MANAGER, EDITOR")]
        public BookForDetailed AddBook(BookForInsert book) => _business.AddBook(Owner, book);

        [HttpPut("{id}")]
        [Authorize(Roles = "MANAGER, EDITOR")]
        public BookForDetailed UpdateBook(int id, BookForInsert book) => _business.UpdateBook(id, Owner, book);

        [HttpDelete("{id}")]
        [Authorize(Roles = "MANAGER, EDITOR")]
        public void DeleteBook(int id) => _business.DeleteBook(Owner, id);
    }
}
