﻿using System;
using System.Collections.Generic;
using System.Data;
using Business.Business;
using Business.Dto;
using Microsoft.AspNetCore.Mvc;

namespace NgaBookAPICore.Controllers
{
    [Route("api/categories")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly ICategoriesBusiness _business;

        public CategoriesController(ICategoriesBusiness business)
        {
            _business = business;
        }

        [HttpGet]
        public IEnumerable<CategoryForDetailed> GetCategories() => _business.GetCategories();

        [HttpGet("{id}")]
        public CategoryForDetailed GetCategory(int id) => _business.GetCategory(id);

        [HttpPost]
        public CategoryForDetailed AddCategory(CategoryForInsert category) => _business.AddCategory(category);

        [HttpPut("{id}")]
        public CategoryForDetailed UpdateCategory(int id, CategoryForInsert category) => _business.UpdateCategory(id, category);

        [HttpDelete("{id}")]
        public void DeleteCategory(int id) => _business.DeleteCategory(id);
    }
}