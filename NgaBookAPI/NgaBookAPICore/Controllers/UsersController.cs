﻿using Business.Business;
using Business.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models.Models;
using System.Collections.Generic;

namespace NgaBookAPICore.Controllers
{
    [Route("api/users")]
    [ApiController]
    [Authorize(Roles = "MANAGER")]
    public class UsersController : ControllerBase
    {
        private readonly IUsersBusiness _business;

        public UsersController(IUsersBusiness business)
        {
            _business = business;
        }

        [HttpGet]
        public IEnumerable<UserForDetailed> GetUsers() => _business.GetUsers();

        [HttpGet("{id}")]
        public UserForDetailed GetUser(int id) => _business.GetUser(id);

        [HttpPost]
        public UserForDetailed AddUser(UserForInsert user) => _business.AddUser(user);

        [HttpPut("{id}")]
        public UserForDetailed UpdateUser(int id, UserForInsert user) => _business.UpdateUser(id, user);


        [HttpDelete("{id}")]
        public void DeleteUser(int id) => _business.DeleteUser(id);
    }
}
