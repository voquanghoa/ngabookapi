﻿using System;
using System.Collections.Generic;
using Business.Dto;
using Business.Business;
using Microsoft.AspNetCore.Mvc;

namespace NgaBookAPICore.Controllers
{
    [Route("api/authors")]
    [ApiController]
    public class AuthorsController : ControllerBase
    {
        private readonly IAuthorsBusiness _business;
        public AuthorsController(IAuthorsBusiness business)
        {
            _business = business;
        }

        [HttpGet]
        public IEnumerable<AuthorForDetailed> GetAuthors() => _business.GetAuthors();

        [HttpGet("{id}")]
        public AuthorForDetailed GetAuthor(int id) => _business.GetAuthor(id);

        [HttpPost]
        public AuthorForDetailed AddAuthor(AuthorForInsert author) => _business.AddAuthor(author);

        [HttpPut("{id}")]
        public AuthorForDetailed UpdateAuthor(int id, AuthorForInsert author) => _business.UpdateAuthor(id, author);

        [HttpDelete("{id}")]
        public void DeleteAuthor(int id) => _business.DeleteAuthor(id);

    }
}