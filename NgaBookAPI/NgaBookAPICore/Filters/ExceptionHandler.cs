﻿using Business.Exceptions;
using log4net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace NgaBookAPICore.Filters
{
    public class ExceptionHandler : IActionFilter, IOrderedFilter
    {
        public int Order => int.MaxValue - 10;

        private readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public void OnActionExecuted(ActionExecutedContext context)
        {
            var exception = context.Exception;

            if(exception is BaseException ex)
            {
                context.Result = new ObjectResult(ex.Message)
                {
                    StatusCode = Convert.ToInt32(ex.StatusCode)
                };
                context.ExceptionHandled = true;
            }

            if (exception != null && !(exception is BaseException))
            {
                log.Error(exception);
            }
        }

        public void OnActionExecuting(ActionExecutingContext context) { }
    }
}
